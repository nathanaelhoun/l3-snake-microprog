MCU	= atmega32u4
TARGET	= snake

LIB_DIR = lib
LIB_OBJ = $(LIB_DIR)/JoystickController.o $(LIB_DIR)/LedController.o $(LIB_DIR)/SpeakerController.o $(LIB_DIR)/LCD.o

all: $(TARGET).elf

$(TARGET).elf: $(TARGET).o libVirtualSerial.a libMicroController.a
	avr-gcc -mmcu=$(MCU) -L. $(TARGET).o -o $(TARGET).elf -lVirtualSerial -lMicroController

%.o: %.c
	avr-gcc -c -mmcu=$(MCU) -Wall -I. -I../lufa-LUFA-140928/ -DF_USB=16000000UL -DF_CPU=16000000UL -Os -std=gnu99 $^ -o $@

libVirtualSerial.a: 
	make -f makefile.lib lib

libMicroController.a: $(LIB_OBJ)
	ar cr $@ $^

clean:
	rm -f *.o lib/*.o libMicroController.a *.elf

DFU=dfu-programmer

flash_dfu:
	@$(DFU) $(MCU) erase
	@$(DFU) $(MCU) flash $(TARGET).hex
	@$(DFU) $(MCU) reset

flash_109: $(TARGET).elf
	avrdude -c avr109 -b57600 -D -p $(MCU) -P /dev/ttyACM0 -e -U flash:w:$(TARGET).elf

