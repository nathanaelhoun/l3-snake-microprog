# Project "Snake"

CMI L3 Informatique - 2020 - UE Micro-prog CMI

## Demo

![Demo](demo.min.mp4)

## Dependencies

- `make`
- `avr-gcc` and `avr-dude`
- [LUFA](http://www.lufa-lib.org), in a directory next to the cloned repository
- This repo includes `libVirtualSeria.a`, which has been given by our teacher to manage the serial port

## Usage

1. Build the project with `make`
2. Flash it on an atmega32u4 with `make flash_109`
3. Have fun!
4. You can also see the progress on the game in `cat /dev/ttyACM0`
