#ifndef HB_BLP_LED_CONTROLLER

#define HB_BLP_LED_CONTROLLER
#define F_CPU 16000000UL
#include <avr/io.h> //E/S ex PORTB

#define NB_LEDS 35
#define NB_LEDS_PER_LINE 5

/**
 * Initialise the microcontroller for the leds
 */
void led_initialise();

/**
 * Change the state of a led in the array
 */
void led_change_state(int *led, int x, int y);

/**
 * Force the led to be off in the array
 */

void led_turn_off(int *led, const int x, const int y);

/**
 * Force the led to be on in the array
 */
void led_turn_on(int *led, const int x, const int y);

/**
 * Show a full frame on the display
 * Takes 35 * 0.5ms to execute
 */
void led_show_frame(const int *leds);

#endif /* HB_BLP_LED_CONTROLLER */