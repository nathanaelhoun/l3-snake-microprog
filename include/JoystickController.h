#ifndef FD_NH_JOYSTICK_CONTROLLER
#define FD_NH_JOYSTICK_CONTROLLER

#define F_CPU 16000000UL
#include <avr/io.h> //E/S ex PORTB

/**
 * Initialise the ADC for compute the value from the joystick
 */
void joystick_adc_init();

/**
 * When a conversion from the ADC is terminated, this function should be called.
 * 
 * It reads in ADMUX which value has been calculated, and update resVertical or 
 * resHorizontal with a value between -3 (for extreme bottom/left) to +3 
 * (for extrem top/right).
 * It also change the value in ADMUX for the next conversion to happen on the 
 * other joystick axis
 */
void joystick_get_directions_and_change_admux(int *resVertical, int *resHorizontal);

#endif /* FD_NH_JOYSTICK_CONTROLLER */