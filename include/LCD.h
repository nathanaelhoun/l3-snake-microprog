#ifndef LC_JD_VP_LCD

#define LC_JD_VP_LCD

#define ARROW_TOP 0
#define ARROW_RIGHT 1
#define ARROW_BOTTOM 2
#define ARROW_LEFT 3
#define LCD_DEAD 4

void lcd_init(void);

void lcd_print(int score, int dir);

void lcd_print_word(int score, char word[]);

#endif /* LC_JD_VP_LCD */