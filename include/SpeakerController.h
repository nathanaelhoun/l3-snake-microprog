#ifndef AC_ML_SPEAKER_CONTROLLER
#define AC_ML_SPEAKER_CONTROLLER

void speaker_init();

void speaker_eat();

void speaker_die();

#endif /* AC_ML_SPEAKER_CONTROLLER */