#define F_CPU 16000000UL

#include <avr/interrupt.h>
#include <avr/io.h>
#include <stdio.h>
#include <util/delay.h>

double fToIcr(double f) {
  return (double)(2000000 / f);
}

void tone(double f) {
  ICR1 = fToIcr(f);
  OCR1A = fToIcr(f) / 2;
  DDRB &= ~(1 << 5);
  _delay_ms(40);
  DDRB |= (1 << 5);
}

void speaker_init() {
  DDRB |= (1 << 5);
  TCCR1A |= (1 << COM1A1) | (1 << WGM11);
  TCCR1B |= (1 << WGM13) | (1 << WGM12) | (1 << CS11);
  ICR1 = 4545; //=440
  OCR1A = (4545 / 2);
  tone(30);
}

void speaker_eat() {
  tone(1046);
  _delay_ms(100);
  tone(1174);
  _delay_ms(100);
  tone(1480);
  _delay_ms(100);
  tone(30);
}

void speaker_die() {
  tone(185);
  _delay_ms(200);
  tone(146);
  _delay_ms(400);
  tone(131);
  _delay_ms(400);
  tone(30);
}
