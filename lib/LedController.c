#include "../include/LedController.h"
#include <avr/io.h> //E/S ex PORTB
#define F_CPU 16000000UL
#include <util/delay.h>

#define DUREE_ALLUMAGE 0.5

void led_change_state(int *led, const int x, const int y) {
  int i = (y * NB_LEDS_PER_LINE) + x;

  if (led[i] == 1) {
    led[i] = 0;
  } else {
    led[i] = 1;
  }
}

void led_turn_off(int *led, const int x, const int y) {
  int i = (y * NB_LEDS_PER_LINE) + x;
  led[i] = 0;
}

void led_turn_on(int *led, const int x, const int y) {
  int i = (y * NB_LEDS_PER_LINE) + x;
  led[i] = 1;
}

void led_initialise() {
  	// Mise de C7 à 1
	DDRC |=1<<PORTC7;
    PORTC |= 1<<PORTC7;
	
	/////////////////
	//   COLUMNS   //
	/////////////////
	
	// D6 = yes
    DDRF |=1<<PORTF1;
    PORTF |= 1<<PORTF1;
    
    // D4 = yes
	DDRD |=1<<PORTD4;
    PORTD |= 1<<PORTD4;
	
	// D0 = yes
    DDRB |=1<<PORTB1;
    PORTB |= 1<<PORTB1;
	
	// B5
	DDRF |=1<<PORTF4;
	PORTF |= 1<<PORTF4;
	
	// D3 = yes
	DDRB |=1<<PORTB3;
    PORTB |= 1<<PORTB3;
	
	////////////////
	//    LINES   //
	////////////////
	
	// B4 = bit a 0
	DDRF |=1<<PORTF5;
	PORTF &= ~(1<<PORTF5);
	
	// C7 = bit à 0
	DDRF |=1<<PORTF0;
	PORTF &= ~(1<<PORTF0);

	// E6 = bit à 0
	PORTC &= ~(1<<PORTC6);
	DDRC |=1<<PORTC6;
		
    // D1 = bit à 0
    DDRB |=1<<PORTB2;
    PORTB &= ~(1<<PORTB2);
	
	// C7 = bit à 0
	DDRC |=1<<PORTC7;
	PORTC &= ~(1<<PORTC7);
	
	// D2 = bit à 0
    DDRB |=1<<PORTB4;
    PORTB &= ~(1<<PORTB4);
	
	// C6 = bit à 0
	DDRE |=1<<PORTE6;
	PORTE &= ~(1<<PORTE6);

	
	PORTF^=1<<PORTF1;
	PORTD^=1<<PORTD4;
	PORTB^=1<<PORTB1;
	PORTF^=1<<PORTF4;
	PORTB^=1<<PORTB3;
	
	PORTF^=1<<PORTF5;
	PORTF^=1<<PORTF0;
	PORTC^=1<<PORTC6;
	PORTB^=1<<PORTB2;
	PORTC^=1<<PORTC7;
	PORTB^=1<<PORTB4;
	PORTE^=1<<PORTE6;

}

void allumer_led(const int n) {
  switch(n){
		   case  0: PORTF^=1<<PORTF5; PORTF^=1<<PORTF1; _delay_ms(0.5); PORTF^=1<<PORTF5; PORTF^=1<<PORTF1;  break;
		   case  1: PORTF^=1<<PORTF5; PORTD^=1<<PORTD4; _delay_ms(0.5); PORTF^=1<<PORTF5; PORTD^=1<<PORTD4;  break;
		   case  2: PORTF^=1<<PORTF5; PORTB^=1<<PORTB1; _delay_ms(0.5); PORTF^=1<<PORTF5; PORTB^=1<<PORTB1;  break;
		   case  3: PORTF^=1<<PORTF5; PORTF^=1<<PORTF4; _delay_ms(0.5); PORTF^=1<<PORTF5; PORTF^=1<<PORTF4;  break;
		   case  4: PORTF^=1<<PORTF5; PORTB^=1<<PORTB3; _delay_ms(0.5); PORTF^=1<<PORTF5; PORTB^=1<<PORTB3;  break;
		   case  5: PORTF^=1<<PORTF0; PORTF^=1<<PORTF1; _delay_ms(0.5); PORTF^=1<<PORTF0; PORTF^=1<<PORTF1;  break;
		   case  6: PORTF^=1<<PORTF0; PORTD^=1<<PORTD4; _delay_ms(0.5); PORTF^=1<<PORTF0; PORTD^=1<<PORTD4;  break;
		   case  7: PORTF^=1<<PORTF0; PORTB^=1<<PORTB1; _delay_ms(0.5); PORTF^=1<<PORTF0; PORTB^=1<<PORTB1;  break;
		   case  8: PORTF^=1<<PORTF0; PORTF^=1<<PORTF4; _delay_ms(0.5); PORTF^=1<<PORTF0; PORTF^=1<<PORTF4;  break;
		   case  9: PORTF^=1<<PORTF0; PORTB^=1<<PORTB3; _delay_ms(0.5); PORTF^=1<<PORTF0; PORTB^=1<<PORTB3;  break;
		   case 10: PORTC^=1<<PORTC6; PORTF^=1<<PORTF1; _delay_ms(0.5); PORTC^=1<<PORTC6; PORTF^=1<<PORTF1;  break;
		   case 11: PORTC^=1<<PORTC6; PORTD^=1<<PORTD4; _delay_ms(0.5); PORTC^=1<<PORTC6; PORTD^=1<<PORTD4;  break;
		   case 12: PORTC^=1<<PORTC6; PORTB^=1<<PORTB1; _delay_ms(0.5); PORTC^=1<<PORTC6; PORTB^=1<<PORTB1;  break;
		   case 13: PORTC^=1<<PORTC6; PORTF^=1<<PORTF4; _delay_ms(0.5); PORTC^=1<<PORTC6; PORTF^=1<<PORTF4;  break;
		   case 14: PORTC^=1<<PORTC6; PORTB^=1<<PORTB3; _delay_ms(0.5); PORTC^=1<<PORTC6; PORTB^=1<<PORTB3;  break;
		   case 15: PORTB^=1<<PORTB2; PORTF^=1<<PORTF1; _delay_ms(0.5); PORTB^=1<<PORTB2; PORTF^=1<<PORTF1;  break;
		   case 16: PORTB^=1<<PORTB2; PORTD^=1<<PORTD4; _delay_ms(0.5); PORTB^=1<<PORTB2; PORTD^=1<<PORTD4;  break;
		   case 17: PORTB^=1<<PORTB2; PORTB^=1<<PORTB1; _delay_ms(0.5); PORTB^=1<<PORTB2; PORTB^=1<<PORTB1;  break;
		   case 18: PORTB^=1<<PORTB2; PORTF^=1<<PORTF4; _delay_ms(0.5); PORTB^=1<<PORTB2; PORTF^=1<<PORTF4;  break;
		   case 19: PORTB^=1<<PORTB2; PORTB^=1<<PORTB3; _delay_ms(0.5); PORTB^=1<<PORTB2; PORTB^=1<<PORTB3;  break;
		   case 20: PORTC^=1<<PORTC7; PORTF^=1<<PORTF1; _delay_ms(0.5); PORTC^=1<<PORTC7; PORTF^=1<<PORTF1;  break;
		   case 21: PORTC^=1<<PORTC7; PORTD^=1<<PORTD4; _delay_ms(0.5); PORTC^=1<<PORTC7; PORTD^=1<<PORTD4;  break;
		   case 22: PORTC^=1<<PORTC7; PORTB^=1<<PORTB1; _delay_ms(0.5); PORTC^=1<<PORTC7; PORTB^=1<<PORTB1;  break;
		   case 23: PORTC^=1<<PORTC7; PORTF^=1<<PORTF4; _delay_ms(0.5); PORTC^=1<<PORTC7; PORTF^=1<<PORTF4;  break;
		   case 24: PORTC^=1<<PORTC7; PORTB^=1<<PORTB3; _delay_ms(0.5); PORTC^=1<<PORTC7; PORTB^=1<<PORTB3;  break;
		   case 25: PORTB^=1<<PORTB4; PORTF^=1<<PORTF1; _delay_ms(0.5); PORTB^=1<<PORTB4; PORTF^=1<<PORTF1;  break;
		   case 26: PORTB^=1<<PORTB4; PORTD^=1<<PORTD4; _delay_ms(0.5); PORTB^=1<<PORTB4; PORTD^=1<<PORTD4;  break;
		   case 27: PORTB^=1<<PORTB4; PORTB^=1<<PORTB1; _delay_ms(0.5); PORTB^=1<<PORTB4; PORTB^=1<<PORTB1;  break;
		   case 28: PORTB^=1<<PORTB4; PORTF^=1<<PORTF4; _delay_ms(0.5); PORTB^=1<<PORTB4; PORTF^=1<<PORTF4;  break;
		   case 29: PORTB^=1<<PORTB4; PORTB^=1<<PORTB3; _delay_ms(0.5); PORTB^=1<<PORTB4; PORTB^=1<<PORTB3;  break;
		   case 30: PORTE^=1<<PORTE6; PORTF^=1<<PORTF1; _delay_ms(0.5); PORTE^=1<<PORTE6; PORTF^=1<<PORTF1;  break;
		   case 31: PORTE^=1<<PORTE6; PORTD^=1<<PORTD4; _delay_ms(0.5); PORTE^=1<<PORTE6; PORTD^=1<<PORTD4;  break;
		   case 32: PORTE^=1<<PORTE6; PORTB^=1<<PORTB1; _delay_ms(0.5); PORTE^=1<<PORTE6; PORTB^=1<<PORTB1;  break;
		   case 33: PORTE^=1<<PORTE6; PORTF^=1<<PORTF4; _delay_ms(0.5); PORTE^=1<<PORTE6; PORTF^=1<<PORTF4;  break;
		   case 34: PORTE^=1<<PORTE6; PORTB^=1<<PORTB3; _delay_ms(0.5); PORTE^=1<<PORTE6; PORTB^=1<<PORTB3;  break;
		   default: break;
	}
}

void led_show_frame(const int *leds) {
  for (int i = 0; i < NB_LEDS; i++) {
    if (leds[i] == 1) {
      allumer_led(i);
    } else {
      _delay_ms(DUREE_ALLUMAGE);
    }
  }
}