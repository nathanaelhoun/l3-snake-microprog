#include "../include/LCD.h"

#include <avr/io.h> //E/S ex PORTB
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <util/delay.h>

#define D0 PORTD3 // D1
#define D1 PORTD5
#define D2 PORTD7 // D6
#define D3 PORTD6 // D12

#define D4 PORTD0 // D3
#define D5 PORTD1 // D2
#define D6 PORTD2 // D0

#define RS PORTB6 // D10
#define E PORTB7  // D11

void write_cmd(char t) {
  PORTB &= ~(1 << RS);

  // D0 -> D3 (lcd)
  if (t & 1) {
    PORTD |= 1 << D0;
  } else {
    PORTD &= ~(1 << D0);
  }
  if (t & 2) {
    PORTD |= 1 << D1;
  } else {
    PORTD &= ~(1 << D1);
  }
  if (t & 4) {
    PORTD |= 1 << D2;
  } else {
    PORTD &= ~(1 << D2);
  }
  if (t & 8) {
    PORTD |= 1 << D3;
  } else {
    PORTD &= ~(1 << D3);
  }

  // D4 -> D6 (lcd)
  if (t & 16) {
    PORTD |= 1 << D4;
  } else {
    PORTD &= ~(1 << D4);
  }
  if (t & 32) {
    PORTD |= 1 << D5;
  } else {
    PORTD &= ~(1 << D5);
  }
  if (t & 64) {
    PORTD |= 1 << D6;
  } else {
    PORTD &= ~(1 << D6);
  }

  PORTB |= 1 << E;

  PORTB &= ~(1 << E);
  _delay_ms(1);
}

void write_data(char t) {
  PORTB |= (1 << RS);

  // D0 -> D3 (lcd)
  if (t & 1) {
    PORTD |= 1 << D0;
  } else {
    PORTD &= ~(1 << D0);
  }
  if (t & 2) {
    PORTD |= 1 << D1;
  } else {
    PORTD &= ~(1 << D1);
  }
  if (t & 4) {
    PORTD |= 1 << D2;
  } else {
    PORTD &= ~(1 << D2);
  }
  if (t & 8) {
    PORTD |= 1 << D3;
  } else {
    PORTD &= ~(1 << D3);
  }

  // D4 -> D6 (lcd)
  if (t & 16) {
    PORTD |= 1 << D4;
  } else {
    PORTD &= ~(1 << D4);
  }
  if (t & 32) {
    PORTD |= 1 << D5;
  } else {
    PORTD &= ~(1 << D5);
  }
  if (t & 64) {
    PORTD |= 1 << D6;
  } else {
    PORTD &= ~(1 << D6);
  }

  PORTB |= 1 << E;

  PORTB &= ~(1 << E);
  _delay_ms(1);
}

void lcd_init(void) {
  _delay_ms(15);

  DDRB |= 1 << RS; //RS
  DDRB |= 1 << E;  //E

  DDRD |= (1 << D0) | (1 << D2) | (1 << D4) | (1 << D5) | (1 << D6) | (1 << D3) | (1 << D1);

  write_cmd(0x38);
  _delay_ms(5);
  write_cmd(0x38);
  _delay_ms(1);
  write_cmd(0x38);
  _delay_ms(1);
  write_cmd(0x3F); // ligne, colonnes
  _delay_ms(1);
  write_cmd(0x0F); // display
  _delay_ms(1);
  write_cmd(0x01); // clear
  _delay_ms(1);
  write_cmd(0x06); // entry set mode
  _delay_ms(1);
}

void lcdw(char mot[]) {
  int l = strlen(mot);
  for (int i = 0; i < l; i++) {
    write_data(mot[i]);
  }
}

void lcd_print(int score, int dir) {
  write_cmd(0x01);
  lcdw(" Scores : ");
  char txt_score[10];
  itoa(score, txt_score, 10);
  lcdw(txt_score);

  for (int i = 0; i < 40 - 9 - strlen(txt_score); i++) {
    write_cmd(0x14);
  }

  lcdw("Direction : ");
  switch (dir) {
  case ARROW_BOTTOM:
    lcdw("V");
    break;
  case ARROW_LEFT:
    lcdw("<");
    break;
  case ARROW_RIGHT:
    lcdw(">");
    break;
  case ARROW_TOP:
    lcdw("^");
    break;

  case LCD_DEAD:
    lcdw("Dead");
    break;
  }
}

void lcd_print_word(int score, char word[]) {
  write_cmd(0x01);
  lcdw(" Scores : ");
  char txt_score[10];
  itoa(score, txt_score, 10);
  lcdw(txt_score);

  for (int i = 0; i < 40 - 9 - strlen(txt_score); i++) {
    write_cmd(0x14);
  }

  lcdw(word);
}