
#include "../include/JoystickController.h"

void joystick_adc_init() {
  ADMUX = (1 << REFS0); // AREF = AVcc, 2.5 V si (1<<REFS0|1<<REFS1)
  // ADC Enable and prescaler of 128 : 16 MHz/128=125 kHz
  ADCSRA = (1 << ADEN) | (1 << ADPS2) | (1 << ADPS1) | (1 << ADPS0);
  ADCSRA |= (1 << ADIE);
}

int direction_v(unsigned short v) {
  int res = 0;
  // v = neutre en 0x020B
  // haut : 0x03FF
  // bas : 0x0000

  if (v < 0x30) {
    // On descend
    res = -3;
  } else if (v < 0xF0) {
    res = -2;
  } else if (v < 0x01BF) {
    res = -1;
  }

  if (v > 0x3F0) {
    // On monte
    res = 3;
  } else if (v > 0x300) {
    res = 2;
  } else if (v > 0x240) {
    res = 1;
  }

  return res;
}

int direction_h(unsigned short h) {
  // h = neutre en 0x01F9
  // droite : 0x03FF
  // gauche : 0x002D

  int res = 0;

  // On va à gauche
  if (h < 0x50) {
    res = -3;
  } else if (h < 0xF0) {
    res = -2;
  } else if (h < 0x01B0) {
    res = -1;
  }

  // On va à droite
  if (h > 0x3F0) {
    res = 3;
  } else if (h > 0x300) {
    res = 2;
  } else if (h > 0x0240) {
    res = 1;
  }

  return res;
}

void joystick_get_directions_and_change_admux(int *resVertical, int *resHorizontal) {
  if ((ADMUX | 0x07) == ADMUX) { // Direction horizontale
    (*resHorizontal) = direction_h(ADC);
    
    ADMUX &= ~(0x07);
    ADMUX |= 0x06;
  } else {
    (*resVertical) = direction_v(ADC);

    ADMUX &= ~(0x06);
    ADMUX |= 0x07;
  }
}
