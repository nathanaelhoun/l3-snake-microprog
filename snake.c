#include "include/JoystickController.h"
#include "include/LCD.h"
#include "include/LedController.h"
#include "include/SpeakerController.h"

#include "include/VirtualSerial.h"
#include <avr/io.h> //E/S ex PORTB
#define F_CPU 16000000UL
#include <stdlib.h>
#include <time.h>

#define FRAMES_BETWEEN_UPDATES 50
#define MAX_X 4
#define MAX_Y 6

#define TRUE 1
#define FALSE 0

enum gameState { RUNNING,
                 DEAD,
                 WON };

extern USB_ClassInfo_CDC_Device_t VirtualSerial_CDC_Interface;
extern FILE USBSerialStream;

/** Handler for the ADC Conversion end */
volatile char flagChangeSpeed = 0;
ISR(ADC_vect) { flagChangeSpeed = 1; }

/**
 * Returns the new position of based on the actual position and the speed
 * If it goes out (0 < value or value > MAX_POS), it returns -1
 */
int compute_position(const int actualPosition, const int speed, const int MAX_POS) {
  int newPos = actualPosition;

  if (speed > 0) {
    newPos = actualPosition + 1;
  } else if (speed < 0) {
    newPos = actualPosition - 1;
  }

  if (newPos > MAX_POS) {
    return -1;
    // newPos = 0;
  } else if (newPos < 0) {
    return -1;
    // newPos = MAX_POS;
  }

  return newPos;
}

void randomPos(int *x, int *y) {
  int newX;
  int newY;
  do {
    newX = rand() % MAX_X;
    newY = rand() % MAX_Y;
  } while (newX == *x && newY == *y);
  *x = newX;
  *y = newY;
}

int main(void) {
  srand(time(NULL));
  rand(); // FIXME (remove when the bugline is fixed)

  int ledGrid[NB_LEDS];
  for (int i = 0; i < NB_LEDS; ++i) {
    ledGrid[i] = FALSE;
  }

  speaker_init();
  led_initialise();
  joystick_adc_init();
  lcd_init();

  SetupHardware();
  CDC_Device_CreateStream(&VirtualSerial_CDC_Interface, &USBSerialStream);
  GlobalInterruptEnable();
  sei();

  ADMUX |= 0x06;         // ADC Conversion on vSpeed
  ADCSRA |= (1 << ADSC); // Launch ADC Conversion

  // Initial values
  int ySpeed = 0, xSpeed = 0;
  int xPos = 2, yPos = 3;
  int xObjective, yObjective;
  randomPos(&xObjective, &yObjective);
  int score = 0;
  int gameState = RUNNING;

  led_turn_on(ledGrid, xPos, yPos);
  led_turn_on(ledGrid, xObjective, yObjective);
  lcd_print_word(score, "Let's begin!");

  int nbFrameSinceLastUpdate = 0;
  while (TRUE) {
    // A new joystick value has been computed
    if (flagChangeSpeed != 0) {
      flagChangeSpeed = 0;
      joystick_get_directions_and_change_admux(&ySpeed, &xSpeed);
      ADCSRA |= (1 << ADSC);
    }

    {
      // Pretty output
      fprintf(
          &USBSerialStream,
          "xSpeed: %i ySpeed: %i\nxPos: %i yPos: %i\nPoints: %i  Game state: %i\n",
          xSpeed, ySpeed,
          xPos, yPos,
          score, gameState);

      fprintf(&USBSerialStream, "-------\n");
      for (int y = 0; y <= MAX_Y; ++y) {
        fprintf(&USBSerialStream, "|");
        for (int x = 0; x <= MAX_X; x++) {
          if (x == xObjective && y == yObjective) {
            fprintf(&USBSerialStream, "T");
          } else if (x == xPos && y == yPos) {
            fprintf(&USBSerialStream, "P");
          } else {
            fprintf(&USBSerialStream, ".");
          }
        }

        fprintf(&USBSerialStream, "|\n");
      }

      fprintf(&USBSerialStream, "-------\n\n");
      // End output
    }

    // Update of player position
    if (gameState == RUNNING && nbFrameSinceLastUpdate >= FRAMES_BETWEEN_UPDATES) {
      nbFrameSinceLastUpdate = 0;

      led_turn_off(ledGrid, xPos, yPos);

      int newXPos = compute_position(xPos, xSpeed, MAX_X);
      int newYPos = compute_position(yPos, -ySpeed, MAX_Y);

      if (newXPos == -1 || newYPos == -1) {
        gameState = DEAD;
      } else {
        if (newXPos == xObjective && newYPos == yObjective) {
          gameState = WON;
        }

        xPos = newXPos;
        yPos = newYPos;
        led_turn_on(ledGrid, xPos, yPos);
      }
    }

    switch (gameState) {
    case DEAD:
      for (int y = 0; y <= MAX_Y; ++y) {
        for (int x = 0; x <= MAX_X; x++) {
          led_turn_on(ledGrid, x, y);
        }
      }
      speaker_die();
      lcd_print_word(score, "Game over");
      break;

    case WON:
      ++score;
      randomPos(&xObjective, &yObjective);
      led_turn_on(ledGrid, xObjective, yObjective);
      gameState = RUNNING;
      lcd_print_word(score, "Take one more!");
      speaker_eat();

    case RUNNING:
    default:
      ++nbFrameSinceLastUpdate;
      break;
    }

    // Light up the led grid
    led_show_frame(ledGrid);

    //  Accept signals from the PC
    CDC_Device_ReceiveByte(&VirtualSerial_CDC_Interface);
    CDC_Device_USBTask(&VirtualSerial_CDC_Interface);
    USB_USBTask();
  }

  return 0;
}
